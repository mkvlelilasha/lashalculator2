package com.example.shuatestaobacalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    lateinit var result: TextView
    private var operand: Double = 0.0
    private var operation = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        result = findViewById(R.id.textView)
    }

    fun numClick(clickedView: View) {
        if (clickedView is TextView) {

            var res = result.text.toString()
            var number = clickedView.text.toString()

            if (res == "0") {
                res = ""
            }

            result.text = res + number

        }
    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {

            var operand = result.text.toString()
            this.operand = operand.toDouble()

            operation = clickedView.text.toString()
            result.text = ""

        }
    }

    fun equalsClick(clickedView: View) {
        if (clickedView is TextView) {

            val secOperand = result.text.toString().toDouble()

            when (operation) {
                "+" -> result.text = (operand + secOperand).toString()
                "-" -> result.text = (operand - secOperand).toString()
                "X" -> result.text = (operand * secOperand).toString()
                "/" -> result.text = (operand / secOperand).toString()
                "%" -> result.text = ((operand * secOperand) / 100).toString()
            }

            if (result.text.toString().toDouble() % 1 == 0.0) {

                result.text = result.text.dropLast(2)

            }

        }
    }

    fun clearClick(clickedView: View) {
        if (clickedView is TextView) {


            result.text = ""


        }
    }

    fun dotClick(clickedView: View) {
        if (clickedView is TextView) {

            val rame = result.text.toString()
            val rume = "."

            result.text = rame.plus(rume)
        }
    }

}